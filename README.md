* docker build -t flask .
* docker run -d -p 5000:5000 flask

* It is necessary to allow "Less secure app access" if you want to use gmail account as a sender.
* https://myaccount.google.com/security "Less secure app access" should be "ON"
