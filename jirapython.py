import json
import requests
from flask import Flask, render_template, jsonify, request
from flask_mail import Mail, Message
from datetime import date

APP = Flask(__name__)
TEMPLATE_MAIN_PAGE = "index.html"

USERNAME = ""   # should be empty
PASSWORD = ""   # should be empty
TOKEN = ""      # should be empty
JIRA_URL = ""  # pass here jira url, like https://jira.example.com/jira/rest/api/2/search?jql=
RECIPIENTS = ['mail@example.com', 'mail@example.com']   # list of recipients
RECIPIENTS_CC = ['mail@example.com', 'mail@example.com']  # list of CC


@APP.route('/', methods=['GET', 'POST'])
def login():
    return render_template('login.html')


@APP.route('/issues', methods=['GET', 'POST'])
def index():

    global USERNAME
    global PASSWORD
    username = request.form['userName']
    password = request.form['userPassword']
    USERNAME = username
    PASSWORD = password
    get_string()
    transfer_data = collect_data()
    dropdown_list = ['Open', 'In progress', 'Done', 'On hold']
    return render_template('index.html', data=transfer_data, dropdown_list=dropdown_list)


def get_string():
    global TOKEN
    JQL = 'assignee = ' + str(USERNAME) + ' AND resolution = Unresolved OR worklogAuthor = ' + str(
        USERNAME) + ' AND (worklogDate >= ' \
                    'startOfWeek() AND worklogDate <= endOfWeek()) '

    REQUEST = requests.get(JIRA_URL
                           + JQL, auth=(USERNAME, PASSWORD))
    TOKEN = json.loads(REQUEST.text)


def create_string(issue_string, status):
    """
    Change this method to concatenate string
    :param issue_string: issue key + summary
    :param status: jira status
    :return: str()
    """
    return issue_string + "; Status: " + status + "\n"


def collect_data():
    list_of_strings = []
    for token in TOKEN['issues']:
        string = token['key'] + " - " + token['fields']['summary']
        list_of_strings.append(string)
    return list_of_strings


@APP.route('/submit', methods=['GET', 'POST'])
def collect_results():
    result = dict(request.form)
    email_text = ""
    for i, token in enumerate(TOKEN["issues"]):
        checked_box_name = "chbx" + str(i + 1)
        if result.get(checked_box_name) is not None:
            issue = token['key'] + " - " + token['fields']['summary']
            email_text += create_string(issue, result["ddl" + str(i + 1)])
    if len(email_text) == 0:
        email_text = "You did not choose any option"
    return render_template('result.html', data=email_text)


@APP.route('/send', methods=['GET', 'POST'])
def send_email():
    print("Email:")
    print(request.form["email_text"])
    username = request.form['userName']
    password = request.form['userPassword']
    body = request.form["email_text"]
    APP.config['MAIL_SERVER'] = 'imap.gmail.com'
    APP.config['MAIL_PORT'] = 587
    APP.config['MAIL_USE_TLS'] = True
    APP.config['MAIL_USERNAME'] = username
    APP.config['MAIL_DEFAULT_SENDER'] = username
    APP.config['MAIL_PASSWORD'] = password
    mail = Mail(APP)
    today = date.today()
    date_now = today.strftime("%m/%d/%Y")
    msg = Message('Daily Scrum Report - DevOps - ' + date_now, recipients=RECIPIENTS, cc=RECIPIENTS_CC)
    msg.body = body
    mail.send(msg)
    print("Sent !")
    return render_template("sent.html")


if __name__ == "__main__":
    APP.run()
