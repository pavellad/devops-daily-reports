FROM python:3.7.1

ENV FLASK_APP "/app/jirapython.py"

RUN mkdir /app
COPY . /app
RUN pip3 install --upgrade pip && \
    pip3 install -r /app/requirements.txt

EXPOSE 5000
CMD flask run --host=0.0.0.0
